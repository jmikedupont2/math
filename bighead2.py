#!/usr/bin/python3
from decimal import Decimal
from gmpy2 import mpz, mpq, mpfr
import numpy as np
import os.path
from PIL import Image as im
size = 480
max_xsize = size

bpath = "frames3"
path = bpath + "c"
if not os.path.exists(path):
    os.makedirs(path)

colors_file="colors2{}.gz".format(max_xsize)
if not os.path.exists("colors{}".format(max_xsize)):
    colors = np.zeros(        
        (max_xsize +1,
         3),
        dtype=np.uint8
    ) # create a simple square
    
    for x in range(max_xsize):
        color = list(np.random.choice(range(127,255,10), size=3))
        colors[x] = color
            
    with open(colors_file,'wb') as o:
        args = np.savez(o, colors=colors)


digits = [str(c) for c in range(10)]

scale= mpz(10)

def render(n, page):
    imgd = np.zeros((size,size,3),dtype=np.uint8)

    for i,l in enumerate(page):
        for j,c in enumerate(l):
            #print(l)
            if c not in digits:
                continue
            
            x = int(c)
            color = colors[x]    
            imgd[i][j][0]= color[0]
            imgd[i][j][1]= color[1]
            imgd[i][j][2]= color[2]
    return imgd
n = 0
res = ""
with open("big.txt") as fi:
    for l in fi:
        page = []
        for c in l:
            if c not in digits:
                continue
            n = n + +1
            #scale = scale * mpz(10)
            res = res + c
    last =""
    print(n)
    print("going to divide", n, res[0:10])
    m = mpq(mpz(1),mpz(res))
    print("going to render")
    m1 = mpfr(m,n*3)
    print("going to convert")
    last1 = str(m1)
    with open("bigout.txt",'w') as fo:
        fo.write(last1)
    stop = len(last1)
    line = ""
    print("Len",stop)
    m = 0 
    for i,b in enumerate(last1):
        if b == 'e':
            stop = i
        if len(line) >= size:               
            page.append(line)
            if len(page) > size:
                page.pop(0)
            name = '%s/%s_%08d.jpeg' % (path, "scan", m)                
            if not os.path.exists(name):
                np1 = render(n,page)
                data = im.fromarray(np1)
                
                data.save(name)
                print(name)
            line = ""
            m = m  + 1
        line = line + b
