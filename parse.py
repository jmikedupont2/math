# read in a sequence of digits and look for simple patterns
import fileinput
digits = 10
input_node = 11
split_node = 12
output_node = 13
asize = output_node

# each node contains the array index of nodes that was the input
# the split field is the digit that split it into a new node
# the output is the index to the new array

def new_node():
    mapp = [-1 for x in range(asize)]
    return mapp

data = [
    new_node()
] # the index contains the pointers to the nodes that have a digit in them

def append_node():
    data.append(new_node())

for line in fileinput.input():
    prev = None 
   
    for c in line:
        c1 = int(c)
        
        if prev is not None:
            if mapp[prev] != c1:
                mapp = data[split_node]  = prev
                append_node()
                old_index = len(data)
                mapp = data[-1]                
                mapp[input_node] = old_index # store the index to the node
                mapp[output_node] = len(data)
            else:
                mapp[prev] = c1
            
        prev = c
        
